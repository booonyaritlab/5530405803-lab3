package pengjam.boonyarit.lab3;

public class GradeEvaluation {
	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Usage:GradeEvaluation <score>");
			System.exit(1);
		}
		double score = Double.parseDouble(args[0]);
		computeGrade(score);
	}

	public static void computeGrade(double score) {
		if (score < 0 || score > 100) {
			System.out
					.print(score
							+ " is an invaild score. Please enter score in the range 0-100");
			System.exit(1);
		}
		String grade = null;
		if (score >= 80)
			grade = "A";
		else if (score >= 70)
			grade = "B";
		else if (score >= 60)
			grade = "C";
		else if (score >= 50)
			grade = "D";
		else
			grade = "F";
		System.out.print("The grade for score " + score + " is " + grade);

	}

}
