package pengjam.boonyarit.lab3;

import java.util.Arrays;

public class SimpleStats {
	public static void main(String args[]) {
		int numGPAs = Integer.parseInt(args[0]);
		double sum = 0;
		double[] nums = new double[numGPAs];
		for (int i = 0; i < numGPAs; i++)
			nums[i] = Double.parseDouble(args[i + 1]);
		System.out.println("For the input GPAs:");
		for (int j = 0; j < numGPAs; j++)
			System.out.print(nums[j] + " ");

		for (int k = 0; k < nums.length; k++) {
			sum += nums[k];
		}
		System.out.println();
		System.out.println("Stats:");
		System.out.println("Avg GPA is " + (sum / numGPAs));
		Arrays.sort(nums);
		System.out.println("Min GPA is " + nums[0]);
		System.out.println("Max GPA is " + nums[nums.length - 1]);

	}
}
