package pengjam.boonyarit.lab3;

public class Permutation {
	static int factn;
	static int factk;

	public static void main(String[] args) {

		if (args.length != 2) {
			System.err.println("Usage:Permutation <n> <k>");
			System.exit(1);
		}
		int n = Integer.parseInt(args[0]);
		int k = Integer.parseInt(args[1]);
		fact(n, k);
		long result = permute(n, k);
		System.out.println(n + "!/(" + n + "-" + k + ")! = " + result);

	}

	public static void fact(int n, int k) {
		factn = 1;
		for (int i = 1; i <= n; i++)
			factn = factn * i;
		factk = 1;
		for (int j = 1; j <= n - k; j++)
			factk = factk * j;

	}

	public static long permute(int n, int k) {
		if (n < 0 || k < 0 || k > n)
			return 0;
		System.out.println(n + "! = " + factn);
		System.out.println("("+ n +"-"+ k + ")! = " + factk);
		return factn / factk;

	}

}