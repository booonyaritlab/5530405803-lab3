package pengjam.boonyarit.lab3;

public class DivisorChecker {
	static int d1, d2, min, max;

	public static void main(String[] args) {
		if (args.length != 4) {
			System.err.println("DivisorCkecker <dicisor 1> <divisor 2> "
					+ "<min number> <max number>");
			System.exit(0);
		}
		d1 = Integer.parseInt(args[0]);
		d2 = Integer.parseInt(args[1]);
		min = Integer.parseInt(args[2]);
		max = Integer.parseInt(args[3]);
		displayNumber();
	}

	public static void displayNumber() {
		for ( int i = min; i <= max; i++){
			if (i % d2 == 0){
				if (i % d1 == 0)
					continue;
			System.out.println(i);
			}
		}
		}
}

