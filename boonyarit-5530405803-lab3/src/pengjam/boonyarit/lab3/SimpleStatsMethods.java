package pengjam.boonyarit.lab3;

import java.util.Arrays;

public class SimpleStatsMethods {
	static int numGPAs;
	static double[] nums;
	static double sum = 0;

	public static void main(String[] args) {
		if (args.length < 2) {
			System.out.println("Usage:<SimpleStats> <numGPAs> <GPA>..");
			System.exit(0);
		}
		numGPAs = Integer.parseInt(args[0]);
		nums = new double[numGPAs];

		acceptInput(args);
		displayStats(nums);
	}
	
	public static void acceptInput(String[] args) {
		System.out.println("For the input GPAs:");
		for (int i = 0 ; i < numGPAs; i++)
			nums[i] = Double.parseDouble(args[i+1]);
		for (int j = 0 ; j < numGPAs; j++)
			System.out.print(nums[j] + " ");
		System.out.println();
		for (int k = 0 ; k < numGPAs; k++) {
			sum += nums[k];
		}
	}

	public static void displayStats(double[] nums) {
		System.out.println("Stats:");
		System.out.println("Avg GPA: is " + (sum / numGPAs));
		Arrays.sort(nums);
		System.out.println("Min GPA is :" + nums[0]);
		System.out.println("Max GPA is :" + nums[nums.length - 1]);

	}

	
}
